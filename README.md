# Dotfiles

This is my personal dotfiles configuration.

## Usage

Clone this repository to `$HOME/.cache/dotfiles`.
Be advised, this particular location is hard-coded in the `dotfiles` script.

Get the repository contents into the home directory, by switching to the
`master` branch.
This should use the `bin/dotfiles` script to do it, but since it's not available yet, check its contents for what to do.

If that last `switch` command fails, it means there's already existing files
that would conflict with the repository files.
The simplest way to solve this is to move the existing files out of the way and
re-run the `switch` command. You can then merge them with the checked out
files.

Afterwards, logout and login.
When using SSH, make sure to kill the SSH connection (`ssh -Oexit $WHERE`), to
guarantee the remote user is completely logged in and out.
This is necessary to run the `systemd-tmpfiles(1)` hook, or you can run it
manually before logging out.

The final tasks are:

- Another checkout the current `HEAD` (to apply smudge filters, if necessary)
- Hide untracked files on this repository

In a nutshell, all this can be done using:

```
$ mkdir -p ~/.cache
$ git clone --bare "$REPOSITORY" "$HOME/.cache/dotfiles"
$ cd
$ git --git-dir="$HOME/.cache/dotfiles" --work-tree="$HOME" switch master
$ systemd-tmpfiles --user --create
$ logout
# Login back again ...
$ dotfiles checkout HEAD -- .
$ dotfiles config --local status.showUntrackedFiles no
$ dotfiles s  # Check there are no changes
```

## Documentation

See [the documentation folder](.local/share/doc/) for more complex and deep documentation.

## Development

Take this as the Pirate's Code: More guidelines than hard-and-fast rules.

- Each commit should touch a single subject.
- Follow the XDG Base Directory Specification to a T. Here's the order of actions to take:
  - Send patches upstream. This includes nagging the developers to pay attention to those changes.
  - Use Environment variables to move the files elsewhere (see the `environment.d` setup)
  - Use symlinks to the files elsewhere. This pollutes the home directory, but
    only with symlinks. An acceptable compromise.

  When in doubt, use the `$XDG_DATA_HOME` location.

  Here are helpful links about XDG Basedir Specification:
  - The Specification itself: https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
  - The Arch Linux wiki: https://wiki.archlinux.org/index.php/XDG_Base_Directory

### Commit Messages

Try to keep the commit messages with the same pattern:

- `Documentation: $DESCRIPTION`: Documentation edits
- `Helper Scripts: $DESCRIPTION`: Changes or addition in `bin`
- `New Config: $DESCRIPTION`: New configuration setup
- `Config:$ID:$DESCRIPTION`: Changes for a specific configuration. The `$ID` is
  a friendly name for the configuration, it's not machine readable.
  Ain't nobody got time to keep an updated list of those IDs.

Any patterns outside this patterns are architectural stuff that might touch many "things".
