# Read the environment.d(5) configuration
for dir in ~/.config/environment.d; do
	if [ -d "$dir" ]; then
		for conf in "$dir"/*.conf; do
			if [ -r "$conf" ]; then
				while read -r line; do
					var="${line%%=*}"
					value="${line#*=}"
					eval "$var=$value"
					export "${var?}"
				done <"$conf"
			fi
		done
	fi
done
