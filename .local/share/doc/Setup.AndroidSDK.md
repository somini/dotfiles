# Android Development

Use `~/.local/lib/android-sdk` as the toplevel directory, put each Android SDK
version on a subfolder. This is the way to allow for multiple versions
installed at the same time.

- Must set `$ANDROID_SDK_HOME`/`$ANDROID_HOME` (both?) to that subfolder

- Setup license BS (WTF!)
   - https://gist.github.com/noamtamim/b04ef67239d2b43638aa5a9a024a1937
   - In case Google DMCA's that:
```bash
# The hashes below are supposed to be taken from
# $ANDROID_HOME/licenses/android-sdk-license
# (This is a circular dependency)
cd "$ANDROID_HOME"
mkdir -p licenses

cat << EOF >> licenses/android-sdk-license
8933bad161af4178b1185d1a37fbf41ea5269c55
d56f5187479451eabf01fb78af6dfcb131a6481e
24333f8a63b6825ea9c5514f83c2829b004d1fee
EOF
```

See https://wiki.archlinux.org/index.php/Android#SDK_packages

- SDK Tools (includes API)

This is enough to develop already (at least you can build `.apk` with Gradle.

- Android Emulator
- System Images
- AVD (some kind of .ini that describes system images. Beats me)

There should be a way to get the Android Emulator running. It's much simpler to
just connect a physical device, should work outside the box.
