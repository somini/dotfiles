# Directory Hierarchy

Based on the `$HOME` directory, this is how all the data is organised on disk.

## bin

Homegrown programs for any purpose.
This path is on `$PATH`, so they are usable directly.

## .cache

The main cache folder. AKA `$XDG_CACHE_HOME`.

## .config

The main configuration folder. AKA `$XDG_CONFIG_HOME`.

### backups.files

A list of files that should really be backed up. This is necessary since XDG is not completely followed.

The format for this file is still in development, as is the backup system.

### bash

All the bash configuration. This is symlinked to the standard locations.

### environment.d

User-wide environment variables, standardised by `environment.d(5)`.

Automatically picked up for user systemd units, each shell should have an implementation of this.

This is implemented for sh in `.profile`.

### git

All the git configuration.

Complex smudge/clean filters for the gitk configuration (see `.config/git/.gitattributes`).

### gnupg

GnuPG configuration.

Includes GPG Agent, key retrieval servers, etc. Doesn't include any private data, not even user names.

### user-tmpfiles.d

User configuration for `tmpfiles.d`.

This is a declarative way of requesting the creation of temporary files. See
the manpage for more details.

## .mozilla

Mozilla Firefox configuration.

This has just the profile setup, and manually updated files:

- `chrome/userChrome.css`: CSS to configure the Firefox interface
- `user.js`: Firefox preferences (see `about:config`). This is a moving target.

**TODO:** Move to `$XDG_DATA_HOME` and symlink

## .local/bin

The programs path for third-party software. This is usually a symlink to a home
installation, if the software allows it.

This path is on `$PATH`, so they are usable directly.

## .local/share

The main data home. AKA `$XDG_DATA_HOME`.

### data-\*

User data for proprietary programs or other programs that only lightly follow the specification. Usually this is symlinked to `XDG_CONFIG_HOME`.
