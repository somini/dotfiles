# Post Install Setup

This has a post-install setup for a brand new computer, after setting up this
repository and getting the `dotfiles` command up and running.
Most of these steps do no apply when restoring from an existing installation.

This was tested on Arch Linux, but most commands should be distro-agnostic,
since this is mostly related to home directory setup.

## GPG

**TL;DR**:
- Generate a GPG key pair, to bootstrap many other programs
- Add it to the git identity setup, for automatic usage

**Links**:
- Arch Wiki: https://wiki.archlinux.org/index.php/GnuPG

All the GPG configuration is already committed in `.config/gnupg`.

### Generate a new key

When starting from nothing, this is what you want. Use:

```
$ gpg --full-generate-key
```

Follow the prompts and the documentation for best practices.

Publish the key on the default keyserver:

```
$ gpg --send-key 0xKEYID
```

### Restore an existing key

On the other machine, export the secret key using:

```
$ gpg --export-secret-keys --armor --output "$OUTPUT" $KEYID
```

Transferring that output file securely between files is left as an exercise to
the reader (I use Syncthing).

On the new machine, import the keyfile, then setup the trust level:

```
$ gpg --import "$OUTPUT"
$ gpg --edit-key KEYID
gpg> trust
Please decide how far you trust this user to correctly verify other users' keys
(by looking at passports, checking fingerprints from different sources, etc.)

  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu

Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y
gpg> save
```

Repeat this for all the required private keys.

### Git Identity Setup

After generating the GPG key, you should add it to the git identity file to
sign git commits automatically. This only applies if the key's email address is
a part of the identity.

Edit `~/.local/share/git-identity` and add (or create) the `signingkey` option:

```
[identity ID]
  name = NAME
  email = EMAIL
  signingkey = KEYID
```

Note that existing repositories will not pickup these changes so they will
require running `git identity ID` once again. This is the only necessary task
to automatically start signing all commits.

Another task should be to upload the key to Github and Gitlab, attached to the corresponding username.

Use `gpg --armor --export KEYID | wl-copy` to copy the public key for the given `KEYID`, and upload it to:

- GitLab: https://gitlab.com/profile/gpg_keys
- GitHub: https://github.com/settings/gpg/new

Note that if you add a new subkey to the same key, instead of generating a new
full key, you will need to remove the existing key and add the new key from
scratch. This is observed on both Gitlab and Github.

## Password Store (pass)

**TL;DR**:
- Initialise the password store to be ready to use

**Links**:
- Arch Wiki: https://wiki.archlinux.org/index.php/Pass

Use a standard(ish) password store that can be used both from the command line,
Android and a `rofi` GUI.

To correctly initialise the repository, use:

```
$ pass init KEYID
$ pass git init  # This might error out with "no email was given and auto-detection is disabled"
$ pass git identity ID
$ pass git init
```

**WARNING**: Make sure you use the GPG KEYID, not the email (which is
supported). This can be the key generated before or any other.

From this point, pass is ready to use.

### Restoring an existing repository

If the repository already exists, you should follow the above guide to get an
empty password repository, already correctly configured.

Add the remote repository when the passwords are kept and merge the changes
locally. If multiple computers have different configurations or private keys,
there will be merge conflicts here, which should be solved first. The easiest
way is to use the same configuration everywhere, so that this merge is clean.

After that is done, push the new changes, and then pull the changes on all
remote repositories to avoid more merge commits.

```
$ cd "$PASSWORD_STORE_DIR"
$ git remote add "$REMOTE" "$REMOTE_URL"
$ git fetch --all
$ git branch --set-upstream-to="$REMOTE/master"  # The branch name might be different
$ git merge -s theirs --allow-unrelated-histories -m "Syncing '$(hostnamectl status --static)'"
$ cd
$ pass git push
```
