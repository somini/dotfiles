# Install Armbian on RockPRO64

Follow the official guide for the latest information
[here](https://www.armbian.com/rockpro64/). This should work out of the box.

## Finish firstboot setup

There's an interactive wizard to finish setting up everything. Login as `root` using the default password `1234` using SSH and it should prompt you for basic settings.

This includes:
- Changing the root password
- Creating a new user (should be `sysadmin` or similar)
  - This is the admin user, with `sudo` access

After that you should:
- Change the hostname: `hostnamectl set-hostname $HOSTNAME`
- Install extra helper packages:
  - eza
  - ripgrep

From this point forward, there's no need to use any account for administration
other than `sysadmin`.

## Update system

Update the system and reboot (if necessary), to confirm that the setup remains functional.

```
# sudo apt update && sudo apt upgrade
```

## Setup etckeeper

On Debian, this should automatically enable the daily timer and create the initial commit.

Install the `etckeeper` package.

## SysAdmin: Setup dotfiles

See https://gitlab.com/somini/dotfiles

## SysAdmin: Setup a text editor

Use full-fat neovim.

- `sudo apt install neovim python3-neovim`
- Setup the nvim repo for sysadmin: https://somini.gitlab.io/dotnvim/
