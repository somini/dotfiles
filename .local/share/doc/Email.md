# Email Handling System

Email is a vast topic with as many solutions as there are people. See
References if you think this is an exaggeration.

This will explain how to organise moderate amounts of email, available offline
and fully integrated with the rest of the system.

## Philosophy / Goals / Non-Goals

- Multiple Accounts.
  - All the multiple accounts are "shared" by a single human. There won't be
    any way to segregate information between all accounts.
- Easily search across the content of all emails
  - Corollary to the previous point, there's a single index for all accounts.
- A way to manually "tag" emails into buckets.
  - This can be implemented as sub-folders, or notmuch tags, or GMail labels.
- Integrate credentials with `pass`
  - This requires a certain schema on the pass passwords
  - This disallows usage of `systemd` units, since it has no access to a
    `$DISPLAY` to ask for the GPG password. This is acceptable.

Workarounds for some limitations:
- If you want to segregate between Personal/Work, configure two usernames on
  the machine itself.

## The System

These are all the moving parts to implement this:

- `~/eMail`: All email data is stored in this folder
- `pass`: Stores the credentials. The schema is simple:
  - Passwords are named `eMail/$ACCOUNT_NAME`, with the following lines:
    - `user`: Email Address
    - `url`: Address for the Web Interface. Optional
- `mbsync`: Retrieves the email from all accounts, into subfolders of the
  toplevel folder i.e. `~/eMail/$ACCOUNT_NAME`.
- `msmtp`: Sends email
- `notmuch`: Index all the email for searching and allow manual and
  semi-automated tagging.
- MUA: TBD, choice between:
  - neomutt: Old and busted...
  - aerc: ...New Hotness
  - alot: notmuch based, should be good
  - claws: GUI, simple
  - astroid: GUI, barebones, dependencies on VTE?
  - Some other notmuch thing. Probably not EMACS-based...
  - thunderbird: GUI, fully featured, too complex

For each part of the system, there are specific notes below:

### mbsync

AKA `isync`.

- Common Configuration: `~/.config/mbsync`
- Account Configuration: `~/.local/share/mbsync/*.mbsync`

Each account should be configured as such:

```
# Account: $ACCOUNT_NAME
IMAPAccount $ACCOUNT_NAME
Host $HOSTNAME
Port 993
UserCmd "pass-field user eMail/$ACCOUNT_NAME"
PassCmd "pass-credential eMail/$ACCOUNT_NAME"
SSLType IMAPS
# SSLVersions TLSv1.2
# PipelineDepth 1

MaildirStore $ACCOUNT_NAME__Local
Path ~/eMail/$ACCOUNT_NAME/
Inbox ~/eMail/$ACCOUNT_NAME/INBOX
SubFolders Verbatim

IMAPStore $ACCOUNT_NAME__Remote
Account $ACCOUNT_NAME

Channel $ACCOUNT_NAME
Far :$ACCOUNT_NAME__Remote:
Near :$ACCOUNT_NAME__Local:
Patterns *
```

Specific accounts might need specific configuration, this is just a generic template.

### msmtp

- Common Configuration: `~/.config/msmtp/config`
- Account Configuration: `~/.local/share/msmtp/*.msmtp`

Each account should be configured as such:

```
account $ACCOUNT_NAME
host $HOSTNAME
port $PORT
tls on
tls_starttls off
eval msmtp-field domain eMail/$ACCOUNT_NAME
auth on
eval msmtp-field user eMail/$ACCOUNT_NAME
passwordeval pass-credential eMail/$ACCOUNT_NAME
from $ACCOUNT_EMAIL
# vim: ft=msmtp
```

`$ACCOUNT_EMAIL` can be discovered with `msmtp-field from eMail/$ACCOUNT_NAME`

Specific accounts might need specific configuration, this is just a generic template.

### notmuch

- Common Configuration: `~/.local/share/notmuch.conf`

## Commands

### `email-list`

List metadata for email system. Use `-h` to check all available options.

### `email-sync`

Synchronise email with `mbsync`. Needs an account name (use `$ACCOUNT_NAME`).

### `email-sync-all`

Synchronise email for all accounts.

### `email-send`

Wrap `mstmp`. Needs an account name (use `-a $ACCOUNT_NAME`).

### `email-send-envelope`

Wrap `email-send` selecting email account on `From` header.

## References

Just some of the resources related to email that I came across:

- https://notmuchmail.org/
- https://aerc-mail.org/
- https://git-send-email.io/
- https://useplaintext.email/
- https://www.claws-mail.org/
- https://neomutt.org/
- http://mutt.org/
- https://www.nongnu.org/nmh/
- https://gnus.org/about.html
- https://git.sr.ht/~migadu/alps
