# tarsnap

- Package Names
  - Arch Linux: `tarsnap`

- Wrappers
  - tarsnystemd: SystemD integration
    - Arch Linux: `tarsnystemd`([somini-aur][somini-aur])
  - tsar: Backup Rotation
    - Arch Linux: `tsar`([somini-aur][somini-aur])

## Configuration

- Docs: https://www.tarsnap.com/documentation.html
- Docs: https://gitlab.com/somini/tarsnystemd#configuration

Run `configuration-tarsnap`. The legacy manual process is:

1. Install the packages

1. Optional: Generate a new key using `tarsnap-keygen`. Refer to the docs. Save
   the newly created key file on a common location, as backup.

1. Optional: Restore an existing key. Run `tarsnap --fsck` to download the
   necessary information about the existing backups. This might take a while.

1. Verify `tarsnap` is working by running `tarsnap --print-stats`.

To run `tarsnystemd` for the user, run `configuration-tarsnystemd` first, to
turn the files in `~/.config/tarsnystemd/*.conf` into proper configurations.

## Tasks

### Run certain backups on session exit

TODO:

Check online with `systemctl is-active -q network-online.target`.

Enable `systemd.service(1)` `ExecCondition` to check.

### Create a new tarsnystemd backup profile

Refer to the documentation: https://gitlab.com/somini/tarsnystemd#configuration

### Rotate old backups

See `tsar`. It works best if all your backups are daily. Use the dry run modes
to double check the deletions.

Note that deleting many small backups is more expensive (in bandwidth) than
just keeping them forever. Stick to the big ones only. With the automatic
de-duplication it's harder to know what backups are the "big ones", but focus
on massive file dumps, full database dumps, and the like.

[somini-aur]: https://pacman.somini.xyz/
