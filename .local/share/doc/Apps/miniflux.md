# Miniflux

- Package Names
  - Arch Linux: `miniflux`([somini-aur][somini-aur])

# Config

- Docs: https://miniflux.app/docs/

1. Install the package.

1. Tweak configuration on `/etc/miniflux.conf`, based on the template in
   [`miniflux/miniflux.conf`](miniflux/miniflux.conf).

1. Optional: Copy
   [`miniflux/miniflux-reset.service`](miniflux/miniflux-reset.service) and
   [`miniflux/miniflux-reset.timer`](miniflux/miniflux-reset.timer) to
   `/etc/systemd/system`.

  Run `systemctl daemon-reload` to load those units. Enable only the timer.

1. Follow the [database configuration][dbconf] documentation.

  Another option is to restore the DB from a backup. See
  [how](postgres.md#Restore a database backup) on the PostgreSQL help.

After that, you can start and enable the `miniflux` service, and the
`miniflux-reset.timer` unit.

## Tasks

[dbconf]: https://miniflux.app/docs/installation.html#database
