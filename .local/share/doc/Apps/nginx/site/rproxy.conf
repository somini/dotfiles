server { # <WHAT>
    server_name <SERVER_NAME>;
    listen 443 ssl;
    listen [::]:443 ssl;
    include conf.d/tls-intermediate.conf;
    include conf.d/common.conf;
    include conf.d/tightsec.conf;

    location / {
        proxy_pass http://<UPSTREAM>/;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Script-Name /; # Same from "location" above
        proxy_set_header Connection "";  # Don't close the connection eagerly
        # Some apps might require this, perhaps?
        #proxy_redirect off;
        #proxy_pass_header Authorization;
    }

    # Logging
    # - Write per-host logs, on the usual location
    # - gzip the logs
    # - Flush every 1h, losing logs is fine by me
    access_log /var/log/nginx/<SERVER_NAME>.log.gz combined gzip flush=1h;

    # TLS
    ssl_certificate /etc/letsencrypt/live/<SERVER_NAME>/fullchain.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/<SERVER_NAME>/chain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<SERVER_NAME>/privkey.pem;
}
