# Certbot

- Package Names
  - Arch Linux: `certbot`

- Optional: DNS Plugins
  - Hurricate Electric (Authenticator: `dns-henet`):
    - Arch Linux: `certbot-dns-henet-git`([somini-aur][somini-aur])
- Optional: Web Root (Authenticator: `webroot`)
  - TLS Redirector
    - Arch Linux: `tls-redirector`([somini-aur][somini-aur])

## Configuration

- Docs: https://certbot.eff.org/docs/
- Arch Docs: https://wiki.archlinux.org/index.php/Certbot

1. Install the package

1. Optional: Install any necessary DNS plugin packages

1. Copy [`certbot/certbot.service`](certbot/certbot.service) and
   [`certbot/certbot.timer`](certbot/certbot.timer) to `/etc/systemd/system`.

  Run `systemctl daemon-reload` to load those units.

1. Install [`certbot/certbot.tmpfilesd.conf`](certbot/certbot.tmpfilesd.conf)
   on `/etc/tmpfiles.d/certbot-dns.conf`.

  Run `systemd-tmpfiles --create` to create the folders.

1. Edit `/etc/letsencrypt/cli.ini`
  - `authenticator` should be a choice between Web Root or a DNS Plugin
  - Optional: Include credentials for DNS plugin:
    - Hurricane Electric:

      ```
      ; dns-henet
      dns-henet-credentials = /etc/letsencrypt/dns-credentials/henet
      ```

      That credentials file should have the following content:

      ```
      dns_henet_username=$USERNAME
      dns_henet_password=$PASSWORD
      ```

  - Optional: Setup webroot:

    ```
    webroot-path = /var/cache/acme-challenge
    ```

    This is the path created and used by `tls-redirector`

  - Reload the server on renewal:

    ```
    ; Reload server on renewal
    deploy-hook = systemctl reload nginx
    ```

    This should be adjusted depending on the server installed.

1. Enable timer to renew certificates: `systemctl enable --now certbot.timer`

You first task right away should be to register an account to be able to
generate new certificates directly.

## Tasks

### Register an account

To register an account to be able to create certificates, use:

```sh
certbot register
```

This is an interactive process, answer the questions as they come.

This task can be skipped, but it will be done as soon as you try to create the
first certificate.

### Create a new certificate

To create a new certificate for one (or more) domains (including wildcards, if the DNS plugin in installed), use:

```sh
certbot certonly -d $DOMAIN [ -d $DOMAIN_EXTRA ]
```

Note that some DNS plugins can have a waiting time of 60 seconds, to propagate
the DNS changes.

This should create a folder with the generated files in
`/etc/letsencrypt/live/$DOMAIN`. Whatever program needs to use this can just
refer to this location.

See also the `deploy-hook` above, to reload the server when the certificates
are renewed. This can be tweaked per-certificate.

[somini-aur]: https://pacman.somini.xyz/
