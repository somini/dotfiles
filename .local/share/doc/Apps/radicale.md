# Radicale

- Package Name
  - Arch Linux: `radicale`

## Configuration

- Docs: https://radicale.org/3.0.html#tutorials/basic-configuration
- Arch Docs: https://wiki.archlinux.org/index.php/Radicale#Configuration

1. Install the package
1. Copy to `/etc/bin`: `radicale-hook` `radicale-shell`
1. Edit `/etc/radicale/config`
  - Server (`server`)
    - `hosts` indicates the server binding
    - Set `max_connections` to roughly the number of users, depending on the
      host resources
    - Set `max_content_length` to 50 MB (`50000000`)
  - Authentication (`auth`)
    - Set `type` to `htpasswd`
    - Set `htpasswd_filename` to `/etc/radicale/users`
    - Set `htpasswd_encryption` to `plain`
    - Set `delay` to `5` seconds (or more)
    - Set `realm` to a server description
  - Rights Management (`rights`)
    - Set `type` to `from_file`
    - Set `file` to `/etc/radicale/rights`
  - Storage (`storage`)
    - `filesystem_folder` indicates the storage for the data
      - To relocate the data, it is better to symlink the default location to
        the relocated location
    - `hook` indicates the command ran after changes. Use
      `/etc/bin/radicale-hook %(user)s`
1. Edit `/etc/radicale/rights`
  - You can copy directly from `radicale/rights`
1. Setup the data location
  - Run `radicale-shell`, and inside run `radicale/radicale-init`
1. Adjust the configuration file permissions
  - Run `chown -vR radicale:radicale /etc/radicale`
  - Run `chmod -vR o= /etc/radicale`
1. Setup backups
  - Configure git to allow commits for a different user: `git config --system safe.directory <WHERE>`
    - Another alternative is to configure `tarsnystemd-pre@pim` to use the `radicale` user.
  - Use `radicale/tarsnystemd.conf` and `radicale/tarsnystemd.pre`, renaming them to `pim`
  - Enable daily backups: `systemctl enable --now tarsnystemd@pim.timer`
1. If forwarding the radicale port with nginx `rproxy.conf`, make sure `proxy_set_header X-Script-Name ""` does not end with `/`.

Make sure the server's hostname is a FQDN, or the commit hook might fail,
resulting in server errors. Another option is to force the git user data:

Run `/etc/bin/radicale-shell`, and inside run:

```sh
git config user.name <USERNAME>
git config user.email <EMAIL>
```

## Tasks

### Move Storage folder

Use the template in
[`radicale/location.tmpfiles.conf`](radicale/location.tmpfiles.conf) to create
a file in `/etc/tmpfiles.d/`, replacing `<WHERE>` for the right location.

Run `systemd-tmpfiles --create` to create the folders.

You also need to open up the permissions on the unit for the new location:

```systemd
# /etc/systemd/system/radicale.service.d/location.conf
[Service]
ReadWritePaths=<WHERE>
```

### Create a new user

Create a new user named `$USERNAME`

#### Setup authentication

Edit `/etc/radicale/users` and add a new line:

```
$USERNAME:$PASSWORD
```

Make sure the password is unique, since it is saved as plaintext on the server.

#### [Optional] Give access to the common collections

Run `radicale-shell`, and inside run:

```sh
cd collection-root/$USERNAME
ln -srv ../_/calendar common-calendar
ln -srv ../_/contact common-contact
```

This gives access to the common calendar and contacts.

### Restore a backup

The backup should be a git repo, accessible to the `radicale` user. You can use
a [git bundle](https://git-scm.com/docs/git-bundle) for this purpose, it can be
directly cloned from as if it was a regular repository URL.

Run `radicale-shell`, and inside run:

```sh
git clone <BACKUP> .
# Remove the remote on the backup file
git remote -v remove origin
```
