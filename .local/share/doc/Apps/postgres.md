# PostgreSQL

- Package Names
  - Arch Linux: `postgresql` `postgresql-old-upgrade`

## Configuration:

- Docs: https://www.postgresql.org/docs/
- Arch Docs: https://wiki.archlinux.org/index.php/PostgreSQL

1. Install the packages
1. Copy to `/etc/bin`: `postgres/postgres-shell`
1. Check the task to [Move Storage folder](#Move Storage folder), it's simpler
   to change it right at the start

1. Follow the [Initial Configuration][iconf] from Arch docs

  - Make sure to use a UTF-8 locale:

  ```sh
  # This is as the "postgres" user
  initdb --locale=en_US.UTF-8 -E UTF8 -D ~/data
  ```

  - Performance: [Don't stress the disk with statistic writes][p:niw]:

  Move the `stats_temp_directory` on the configuration file
  (`~postgres/data/postgresql.conf`) to `/run/postgresql`.

After that, you can start and enable the `postgresql` service.

## Tasks

### Move Storage folder

Change the `PGROOT` variable, and the PID file location along with it. This is
documented [here][mdata].

1. Use the template in
   [`postgres/db.tmpfilesd.conf`](postgres/db.tmpfilesd.conf) to create a file
   in `/etc/tmpfiles.d/db.conf`, replacing `<WHERE>` for the right location.

1. Create a file in `/etc/systemd/system/postgresql.service.d/location.conf`:

  ```
  [Service]
  Environment=PGROOT=<WHERE>
  PIDFile=<WHERE>/data/postmaster.pid
  ```

1. Create the target folder with `systemd-tmpfiles --create`.

1. Optional: Change the home location for the `postgresql` user to the `PGROOT` location.

  Run `vipw` to edit the `/etc/passwd` file, don't edit it directly or you
  might get locked out.

After this, the [Configuration](#Configuration) needs to be done again from scratch.

### Restore a database backup

Make sure the database daemon is running.

Run `/etc/bin/postgres-shell`, and inside:

```sh
psql -f <FILE_TO_RESTORE>
```

That file is text, so it should compress very well, for storage.

### Upgrade between major versions

Upgrades between minor versions just need a daemon restart, but between major
versions a manual process is needed. This is documented [here](upgrade).

<!-- TODO: write some lines about this? Automate? -->

[iconf]: https://wiki.archlinux.org/index.php/PostgreSQL#Initial_configuration
[mdata]: https://wiki.archlinux.org/index.php/PostgreSQL#Change_default_data_directory
[p:niw]: https://wiki.archlinux.org/index.php/PostgreSQL#Prevent_disk_writes_when_idle
[upgrade]: https://wiki.archlinux.org/index.php/PostgreSQL#Upgrading_PostgreSQL
