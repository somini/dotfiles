# Nginx

- Package Names
  - Arch Linux: `nginx`

## Configuration

- Docs: https://nginx.org/en/docs/
- Arch Docs: https://wiki.archlinux.org/index.php/Nginx

The packages usually come with an existing minimal (or not) configuration, but
this will document the configuration from scratch. There's no need to include
more configuration than needed.

The configuration is rooted in `/etc/nginx`.

1. Install the package

1. Setup the main configuration file, located in `/etc/nginx/nginx.conf`.

  Use [`nginx/nginx.conf`](nginx/nginx.conf), it is properly commented.
  Please read and adjust, based on the hardware.

1. Setup the configuration fragments, located in `/etc/nginx/conf.d`.

  Copy the [`nginx/conf.d`](nginx/conf.d) folder.

1. Generate the default self-signed certificates.

  - Create a certificate folder with `mkdir -p /etc/nginx/certificates`.

  - Generate the certificates by running `nginx/generate-selfsignedcerts`.

  This is necessary to setup a default bogus server, to whitelist the
  served hostnames.

1. Get the latest Diffie-Hellman parameters. Run one of the commands.

  ```
  # Trust Mozilla
  curl 'https://ssl-config.mozilla.org/ffdhe2048.txt' >/etc/nginx/certificates/dhparam
  # Generate locally
  openssl dhparam -out /etc/nginx/certificates/dhparam 2048
  ```

## Tasks

### New Site

There are several types of site, but all involve common tasks:

- Create a CNAME to the "real" host
- [Create a new certificate with certbot](certbot.md#Create a new certificate),
  and reload `nginx`.
- Add a file on `/etc/nginx/site`. Each type is documented below:

#### Local Static Files

To serve static files from a directory, use the
[`nginx/site/static.conf`](nginx/site/static.conf) template.

Assuming the server name is `<SERVER_NAME>`:
- The files are located in `/srv/http/<SERVER_NAME>`
- The logs are stored in `/var/log/nginx/<SERVER_NAME>.log.gz`
- The TLS certificate is stored in `/etc/letsencrypt/live/<SERVER_NAME>`

#### Reverse Proxy other server

To reverse proxy another server, use
[`nginx/site/rproxy.conf`](nginx/site/rproxy.conf). Usually this is a server
listening on the local machine, use a non-standard but still `localhost` IP
like `127.0.1.0`. This avoids DNS races on boot, and improves security (through
obscurity).

Assuming the server name is `<SERVER_NAME>`:
- The upstream server is `<UPSTREAM>`. This should be an `IP:port` or
  `name:port` string. The `:port` is optional, default to `80`.

If the other server is running on the same machine, order `nginx` after the
proxy server, to avoid race conditions on startup.

Edit the `systemd/system/nginx.service.d/dependencies.conf` file and add something like:

```
[Unit]
After=<upstream>.service
```

#### Basic Authentication

To add basic authentication to any site, add the following lines to the configuration:

```
# Basic Authentication
auth_basic "<SITE_DESCRIPTION>";
auth_basic_user_file <SITE_NAME>.auth;
```

Assuming the site name is `<SITE_NAME>`, and a description is `<SITE_DESCRIPTION>`.

The authentication file `/etc/nginx/<SITE_NAME>.auth` can have the following contents:

```
# Comment
username1:{PLAIN}password1_plaintext
```

This make it easy to add more users, and the passwords are in plain text,
simple stuff.

## Future Work

It would be interesting to move this to a couple of smaller programs:

- [tlstunnel](https://git.sr.ht/~emersion/tlstunnel) to forward TLS to HTTP
  servers, and deal with Let's Encrypt automatically (also deprecates
  [certbot](certbot.md).

  These are basically the only features implemented here.

- [kimchi](https://git.sr.ht/~emersion/kimchi) to serve static HTML files.

  This could be done now, but the following issues are blocking:

  - Support compression: https://todo.sr.ht/~emersion/kimchi/15
  - Configure proxy timeouts: https://todo.sr.ht/~emersion/kimchi/12
