# Running Bibliogram on NearlyFreeSpeech

Bibliogram is an Instagram interface, written in node.

- Project: https://sr.ht/~cadence/bibliogram/
- Installation Docs: https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Installing%20%28extended%29.md

Life is too short to run [JavaScript webshit](http://n-gate.com/) on my own
infrastructure, outsource that!

## Configuration

1. Start a new site, type "Apache 2.4 Generic". This includes only Apache as a
   reverse proxy.

1. Install bibliogram. Follow the instructions:

   ```sh
   cd /home/private
   git clone https://git.sr.ht/~cadence/bibliogram
   cd bibliogram
   npm install --no-optional
   ```

2. Configure the application by editing `config.js`.

   Refer to the [upstream documentation][config]. The important parts are:
   - `website_origin`: The external URL
   - `port`: The local port to bind, used below

1. Setup the main launcher program:

   ```sh
   cat >/home/private/main.sh <<-RUN
   #!/bin/sh
   exec npm run start
   RUN
   chmod +x /home/private/main.sh
   ```

3. Setup the "main" daemon. Create a new NFS daemon:

  - Command Line: `/home/private/main.sh`
  - Working Directory: `/home/private/bibliogram`
  - Run Daemon As: `me`

3. Setup the well known proxy. Create a new NFS proxy:

  - Protocol: `none`
  - Base URI: `/.well-known/`
  - Document Root: `/home/public/`

  This is necessary to make TLS work, by skipping the app for the "Base URI" prefix.

3. Setup the main proxy. Create a new NFS proxy:

  - Protocol: `HTTP`
  - Base URI: `/`
  - Document Root: `/`
  - Target Port: Depends on the configuration above

  This proxies the actual application. The port to forward depends on the configuration.

This should make it possible to access the site. Afterwards, configure TLS
before putting it in "production".

## Notes

bibliogram has a binary dependency, some kind of SQLite fork or something
*shudder*. This means if the underlying node version changes, recompilations
are required. You can "test" for this by changing the NFS "Sofware Realm".
When this happens, just run `npm clean-install` to recompile everything.
This uses the locked package versions, so it should be "safe", no borkage is
expected.

[config]: https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Configuring.md
