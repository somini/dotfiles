# Transmission

- Package Names
  - Arch Linux: `transmission-cli`

## Configuration

- Docs: https://trac.transmissionbt.com/wiki/HeadlessUsage/General
- Arch Docs: https://wiki.archlinux.org/title/Transmission

1. Install the packages
1. Start and stop the daemon to create a configuration file

  ```sh
  systemctl start transmission && systemctl stop transmission
  ```

1. Create necessary with the right permissions

  This includes the download directory.

  - Use the template in
    [`transmission/location.tmpfiles.conf`](transmission/location.tmpfiles.conf)
    to create a file in `/etc/tmpfiles.d/`, replacing `<WHERE>` for the right
    location.

  Run `systemd-tmpfiles --create` to create the folders.

  - Add the users you want to modify the files to the `transmission` group.
    This is optional, they are world readable and can be managed using the
    interfaces.

1. Tweak the settings

  Edit `/etc/transmission.json`:

  - `download-dir`: Use `<WHERE>` configured above
  - `incomplete-dir`: Use `<WHERE>` configured above
  - `peer-port-random-on-start`: Enable
  - `rpc-bind-address`/`rpc-port`/`rpc-url`: use a localhost address (`127.0.1.0`/`15004`/`/`)
  - `rpc-whitelist-enabled`: Disable, it's already controlled by the bind address
  - `rpc-host-whitelist`: The hostname where all RPC requests must originate.

  Check the scheduling documentation to do the bulk of the downloads at night.

  - [Scheduling](https://github.com/transmission/transmission/blob/main/docs/Editing-Configuration-Files.md#scheduling)

1. Enable and start the daemon

  ```sh
  systemctl enable --now transmission
  ```

1. Check if the daemon is correctly configured

  Use the RPC settings:

  ```sh
  transmission-remote http://127.0.1.0:15004/rpc -l
  ```

1. Follow the [nginx docs](Apps/nginx.md) to create a New Site, as a Reverse Proxy.

  Include credentials to avoid exposing the interface to the world. It's easier
  to configure on the server, rather than on the app itself.

  Include this redirect as the first block:

  ```
  location = / {
      return 302 /web/;
  }
  ```

## Tasks

### Configure RPC

After configuring the reverse proxy for remote access, here are the settings to
use on remote RPC clients, like `transmission-remote-gtk`:

- Host: `$WHERE`, the public DNS name
- Port: `443`, HTTPS
- RPC URL Path: `/rpc`
- Credentials: Check the nginx configuration
- SSL: Enable
- Validate SSL Certificate: Enable
