# Firefox

- Package Names: `firefox.packagelist`

## Configuration

- Arch Docs: https://wiki.archlinux.org/index.php/Firefox

1. Install the packages

1. Open Firefox for the first time

1. Install the missing extensions

1. Configure Firefox

  - Disable Picture-in-Picture
  - Disable "Control media"
  - Disable recommendations

  - Show "Firefox Home" on Homepage and New Tab
  - Firefox Home Content: Show only Web Search

  - Add search bar in toolbar
  - Remove all Search Engines except DuckDuckGo
  - Disable all Search Suggestions

  - Disable "Logins and Passwords"

  - Disable "Top Sites" on Address Bar
  - Disable "Block dangerous and deceptive content"
  - Enable HTTPS only mode (can be overridden per-site)

1. Configure the extensions

When installing new extensions, they all can work on Private Mode.

Here are the per-extension settings:

  - Auto Tab Discard
    - Discard Inactive Tabs after 15 min
    - Change favicon of discarded tabs
    - Do not discard a tab when it's pinned
    - Disable "Open FAQs page on updates"
  - Context Search Origin
    - Set "Open search results in current tab"
    - Disable Multiple Search Results
    - Disable Favicons
  - Clear URLs
    - Disable ETag clearing
  - Cookie AutoDelete
    - Automatic Cleaning: 5 seconds
    - Other Browsing Data Cleanup
      - IndexedDB
      - LocalStorage
      - Plugin Data
      - Service Workers
    - Enable Cleanup Log and Counter
    - Disable Notifications for Automatic Cleanups
    - Disable Context Menus
  - Feed Preview
    - Remove all Feed Readers and add your personal:
      - Miniflux: `$HOST/bookmarklet?uri=%s`
  - Temporary Containers
    - Advanced > General > Popup: Show icon on the address bar that reveals the popup
    - Random Container Colour
    - Container Number: Reuse Available Numbers
  - uBlock Origin
    - Advanced User
    - Block Media larger than 75 KB
    - Block remote fonts
    - Disable JavaScript
    - Add more Filter Lists
      - AdGuard Base
      - AdGuard Annoyances
      - AdGuard Social Media
      - Fanboy's Annoyances
      - Fanboy's Social
      - EasyList Cookie
      - uBlock filter - annoyances
      - spa, por: AdGuard Spanish/Portuguese
  - LocalCDN
    - Silent Updates
    - Disable HTML filter
    - Setup the "uBlock Origin" rules

### Extensions

- Auto Tab Discard
* AutoAuth
- Clear URLs
- Context Search Origin
- Cookie AutoDelete
* Dark Reader
- LocalCDN
  - Older Version: Decentraleyes
- Feed Preview
- Tab2QR
- Temporary Containers
* Tree Style Tab
* TST Mouse Wheel and Double Click
- Sidebery
- uBlock Origin
- Undo Close Tab
