# Install Arch Linux (Generic Guide)

## Installation Guide

Follow the official guide for the latest information
[here](https://wiki.archlinux.org/index.php/Installation_guide).
Make sure to follow each link/rabbit hole.

These are only extra notes about the installation.

### Boot the live environment

Some problems might arise on first boot, since there are no GPU drivers available:

- Use `nomodeset` on the first boot. See
  [KMS](https://wiki.archlinux.org/index.php/Kernel_mode_setting#Disabling_modesetting)

### Partition the Disks

Follow https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#Encrypted_boot_partition_(GRUB)

- Create two partitions:
  - EFI System Partition
  - Linux LUKS
- Create a LUKS container (hardened):
  - `--key-size 512`
  - `--iter-time 5000`  # 5 seconds
  - `--hash sha512`
  - Include a `--label` with the HostName
- Open the LUKS container
  - If the underlying disk is an SSD, use `--allow-discards --persistent` when `open`ing it.
  - Include the HostName on the opened container name (useful for disambiguation for recovery)
- Create the LVM volumes inside the opened LUKS container
  - Name the Volume Group with the HostName

- Include nicer labels:
  - FAT: `-n LABEL`
  - Swap: `-L LABEL`
  - ext4: `-L LABEL`
  - F2FS: `-l LABEL`
  - BTRFS: `-L LABEL`

See also:
- Setup TRIM on SSD: https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#Discard/TRIM_support_for_solid_state_drives_(SSD)

### Install extra packages

After "Install essential packages", install other important packages from the start.

- `base-devel`
- etckeeper
  - `etckeeper`
  - `git`
- Filesystem-related tools
- Network-related packages:
  - `iwd`
  - `iw`
  - `openssh`, optional
    - Required without remote access (AKA cloud), needs to be enabled on first boot
- pacman-related tools
  - `reflector`
- Nicer utilities
  - `eza` (ls)
  - `ripgrep` (grep)
  - `fd` (find)

See the [main package list](ArchLinux.packagelist), and the [specific list for
desktop usage](ArchLinux.desktop.packagelist).

### Setup etckeeper

This should be done ASAP, ideally after installing the extra packages.

- Chroot to the installed system and setup etckeeper:
  - `hostnamectl set-hostname $HOSTNAME`
  - `etckeeper init`
  - `cd /etc && git config --local user.name 'somini' && git config --local user.email 'dev@somini.xyz'`
    - This is just so that git shuts up about the username
  - `etckeeper commit 'Initial Commit'`
- Configure UTC timezone: `ln -svf /usr/share/zoneinfo/UTC /etc/localtime`

- Remove the spurious changes
  - `*-`, the user backup files
  - `.updated`, written by SystemD
  - `pacman.d/mirrorlist`, updated by Reflector

### Setup reflector

This should remove the hassle of updating the mirror list and making sure package download is performant

- Configure reflector in `/etc/xdg/reflector/reflector.conf`
  - Setup a country list (Optional)
  - Change the numbers of mirrors to include (20)
- Enabled the `reflector.timer` to update the list automatically

### Network Configuration

Use `iwd` for WiFi, `systemd-networkd` for everything else. Use
`systemd-resolvectl` as DNS client.

Configure pretty interface names on `/etc/systemd/network/*.link`

See:
- https://wiki.archlinux.org/index.php/Systemd-networkd#Renaming_an_interface
- https://wiki.archlinux.org/index.php/Systemd-networkd#[Link]
- https://wiki.archlinux.org/index.php/Systemd-networkd#[Network]

Install `wireguard-tools` for userspace support for Wireguard.

For DNS, use `systemd-resolved`. Setup the stub resolver with:

```sh
# ln -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```

See:
- https://wiki.archlinux.org/index.php/Systemd-resolved

#### Configure network domains

If the computer will be using trusted networks, you can enable trusting the
network for the current domain name. Append the following to the
`/etc/systemd/network/*.network` files:

```
[DHCPv4]
UseDomains=yes
[IPv6AcceptRA]
UseDomains=yes
```

Reboot and `resolvectl` will pick up the domain names from DHCP.

### initramfs

AKA `mkinitcpio`.

See:
- https://wiki.archlinux.org/index.php/Mkinitcpio#Image_creation_and_activation
- https://wiki.archlinux.org/index.php/Dm-crypt/System_configuration#mkinitcpio

### Bootloader

Use `systemd-boot`

See:
- https://wiki.archlinux.org/index.php/Dm-crypt/System_configuration#Using_sd-encrypt_hook

### Setup a text editor

Crucial for regular system administration. No reason why that is not neovim
with all the bells and whistles (except perhaps performance on very low powered
machines, include vim too in those cases).

- `pacman -S neovim python-neovim`
  - It's not worth it to use neovim without Denite
- Setup the nvim repo for root: https://gitlab.com/somini/dotnvim
  - The "Full" package, don't clone dozens of git repositories needlessly
  - See: https://somini.gitlab.io/dotnvim/
    - `mkdir ~/.config`
    - `curl -L "https://somini.gitlab.io/dotnvim/Full.tar.gz" | tar xzvf - -C ~/.config`
    - `~/.config/nvim/bin/nvim-reload`

### Setup your custom repository (if exists)

See https://pacman.somini.xyz/ for mine.

### Setup dotfiles

For root too, since this brings quality-of-life improvements and is tested in more places.

See https://gitlab.com/somini/dotfiles

### Security

- Relax the faillock module, defaults to a very strict policy.
  - https://wiki.archlinux.org/index.php/Security#Lock_out_user_after_three_failed_login_attempts

## General Recommendations

After the installation guide is done and you can boot to a root shell, follow
the General Recommendations [here](https://wiki.archlinux.org/index.php/General_recommendations).
Follow some rabbit holes too, but there's no need to go crazy here.

### Time Synchronisation

Run `timedatectl set-ntp true` to enable the NTP client.

### SSD Handling

- Enabled periodic TRIM with `fstrim.timer`

### System Administration

- Setup your username:

```sh
$ useradd -m -Gwheel $NEW_USERNAME
$ passwd $NEW_USERNAME
```

- Setup `sudo`
  - Allow everybody on `wheel` group to run any program: `%wheel ALL=(ALL) ALL`
- Setup `doas` (for the kicks)
  - Allow everybody on `wheel` group to run any program: `permit :wheel as root`

### GPU Support

This is a whole other kettle of fish.

The common stuff first, important packages:
- Mesa
- Vulkan
  - `vulkan-tools vulkan-icd-loader`

#### AMDGPU

This is the recent FLOSS AMD drivers. Should work outside the box-ish.

https://wiki.archlinux.org/index.php/AMDGPU

To get the whole shebang, install `mesa vulkan-radeon amdvlk libva-mesa-driver mesa-vdpau`.
- OpenGL
- Vulkan
- Hardware Video Acceleration (both API)
  - VA
  - VDPAU

Outstanding bugs:
- https://gitlab.freedesktop.org/drm/amd/-/issues/490
  Set `options amdgpu dc=0` on `/etc/modprobe.d/amdgpu.conf` to fix this. Leads
  to no HDMI audio, fair enough for desktops.

### Display Manager

I'll use Sway, logging in with `greetd` (on AUR, but available on my custom repository)

- Install and configure greetd for tty login. `budgie-backgrounds` has nice
  background images.

```sh
# pacman -S greetd cage greetd-gtkgreet budgie-backgrounds
# systemctl enable greetd
```

- Configure greeter user

  - Create user folder, configure it:

```sh
# cat >/etc/tmpfiles.d/greetd.conf <<TMPFILES
d /var/lib/greetd 0750 greeter greeter -
TMPFILES
# systemd-tmpfiles --create
# usermod -d /var/lib/greetd greeter
```

  - Edit the user folder with corresponding settings. This example configures
    dark mode.

```sh
# su - greeter
$ mkdir -p ~/.config/gtk-{3,4}.0
$ cat >.config/gtk-3.0/settings.ini <<GTK3
[Settings]
gtk-application-prefer-dark-theme = true
gtk-button-images = true
gtk-menu-images = true
GTK3
$ cat >.config/gtk-4.0/settings.ini <<GTK4
[Settings]
gtk-application-prefer-dark-theme = true
GTK4
```

- Configure `greetd-gtkgreet`

```sh
cat >/etc/greetd/gtkgreet.css <<CSS
window {
    background-image: url("file:///usr/share/backgrounds/budgie/apollo-11-earth.jpg");
    background-size: cover;
    background-position: center;
}
CSS
# cage -s -- gtkgreet -s /etc/greetd/gtkgreet.css
```

This is enough to have a `getty`-like experience, but graphical, with a nice
background and dark widgets for usage in dark environments.
There's also `greetd-regreet`, but that seems to have some interactions with
`cage`, should be resolved in further versions.

#### Sway "Desktop"

- Install and setup Sway

Use the package list for desktop usage (see also `pacman-packagelist`).

Test this by logging in to a VT and run `sway`

- Configure greetd to launch Sway

TBD

- Once `sway` launches correctly, configure `greetd` to launch it.

```sh
# echo 'sway' >> /etc/greetd/environments
```

### Optional

#### Printing

Install the `cups` (and `hplip` for HP printers) to setup CUPS. Start the `org.cups.cupsd` service.

See https://wiki.archlinux.org/index.php/Cups

## See also

Some assorted links that might be useful.

- https://austinmorlan.com/posts/arch_linux_install/
- https://fogelholk.io/installing-arch-with-lvm-on-luks-and-btrfs/
- https://flypenguin.de/2018/01/20/arch-full-disk-encryption-btrfs-on-efi-systems/
- https://wiki.archlinux.org/index.php/Swap#Swap_file_creation
