# Install Kodi on a Raspberry Pi 4B

## Installation Guide

Follow [the official guide][rpi4-guide] to setup an ArchLinux ARM system, see below.

For a Raspberry 4B, use the `ARMv7` version and **NOT** the `aarch64` variant.
We require full GPU access for video acceleration.

### Defaults

The defaults for the system images are:

- Hostname: `alarmpi`

- User `alarm`:`alarm`
- User `root`:`root`

### Follow official guide

First, follow the [official guide][rpi4-guide] to setup an ArchLinux ARM
system, but use 1 GB for the boot partition, to have some headroom.

If the SD card is called `/dev/sdX`:

- `fdisk /dev/sdX`: Partition the SD card into
  - `1 GB`: 1 GB boot partition
  - the rest, root partition
- Create filesystems on the partitions
  - `mkfs.fat -v -F32 -n KODI-BOOT /dev/sdb1`
  - `mkfs.f2fs -l KODI-ROOT -O extra_attr,inode_checksum,sb_checksum,compression /dev/sdb2`: Use [F2FS][arch-f2fs] for the root partition, supporting extra checksums and compression.
- `cdtemp`: Create a new temporary folder, and move there
- `mkdir boot root`
- Mount the boot and root partitions on the new folders
- Unpack the tarball to the root filesystem
- Move the boot files to the boot filesystem
- `sync` all data
- Move the SD card to the hardware, apply Ethernet and power

After this, it should be possible to access the system using `ssh alarm@alarmpi`. As a first pass:

- Switch user to root: `su -`
- Setup the pacman keyring: `pacman-key --init && pacman-key --populate archlinuxarm && pacman -Sy`
- Install the bare minimum missing packages: `pacman -S f2fs-tools rpi-eeprom`
- Update the system: `pacman -Syu`
- Update the EEPROM firmware: `rpi-eeprom-update -a`
- Reboot, if the kernel was updated

### Setup for Kodi with NAS access

This assumes the appliance itself is "dumb", containing only the software and
system data. All media and even kodi configuration data are stored on a remote
NAS (referred to as `data.lan`), mounted using NFS.
This allows for simple cloning the SD card into copies, without data loss, for
quick recovery after SD card breakage (which will inevitably occur).

See also [the guide here](Install.ArchLinux.md). Some of the points do not apply.

Preliminary reports show that 8GB are enough to store everything, including 1GB
for boot, 2GB of cache, and 1GB of logs.
With further trimming, this could be reduced further.

- After following the previous section, you should have the system up and
  running with the default settings. It should be OK to perform all the rest of
  the setup remotely using SSH.
- Install and setup etckeeper: `etckeeper`
  - Ignore extra files: user/group backups, systemd .updated
  - Remove the extra files from the repo: `git rm --ignore-unmatch --cached *- .updated`
- Setup locale
- Install and setup [dotfiles][somini-dotfiles] for root
- Install and setup [dotnvim][somini-dotnvim] for root
  - Install packages `pacman -S neovim python-pynvim`
- Tweak pacman settings
  - Enable colour, verbose package lists, parallel downloads
  - Setup my [own][somini-pacman] package repository.
- Install and setup doas
- Setup `sysadmin` user: `useradd -m -G wheel sysadmin && passwd sysadmin`
- Reboot and relogin as sysadmin user
- Change the root password
- Remove the `alarm` user: `userdel -r alarm`
- Setup timezone: `timedatectl set-timezone "$(tzselect)" && timedatectl set-ntp true`
- Setup NFS client support
  - Install packages: `pacman -S nfs-utils`
  - See https://wiki.archlinux.org/title/NFS
- Setup NFS mountpoints
  - Data in `/srv/data`
  - Kodi home in `/var/lib/kodi`
- Reboot to check if NFS is correctly setup
- Configure the `kodi-logger` hack
- Install and configure the kodi packages: `pacman -S kodi-rpi kodi-eventclients kodi-logger`
  - Tweak `/boot/config.txt` to include the kodi-specific changes. Append:

  ```
  [all]
  include kodi.config.txt
  ```

  - Change the kodi user/group ID to 90210: `usermod -u 90210 kodi; groupmod -g 90210 kodi`
  - Start the SystemD services: `systemctl enable kodi kodi-logger.timer`
- Install optional kodi dependencies: `pacman -S python-pybluez`
- Avoid mkinitcpio changes:

  ```
  mcd /etc/pacman.d/hooks
  for hook in 90-linux-rpi.hook 90-mkinitcpio-install.hook; do ln -vs /dev/null "$hook"; done
    '90-linux-rpi.hook' -> '/dev/null'
  ```
- Reboot for the boot configuration to be applied

From this point, Kodi should start up as usual, with the same settings as
before since they are located on the shared NFS folder.

Heads up:

- The `kodi-logger` service might fail the first time, because it might run
  before `kodi` runs the first time. This is fine on the first boot, just use
  `systemctl reset-failed` to ignore it.


## Setup Kodi for internet access

You can expose the Kodi web interface securely to the internet using HTTPS and
HTTP Basic Authentication as ACL. This allows access to all Kodi media (and
some extra features, like music's party mode) from any browser in the internet.

On-Premises Netflix + Spotify ON STEROIDS!

This is optional, but massively cool.

- Register a domain/subdomain, get a TLS certificate
- Configure nginx to proxy the kodi interface

  Assuming the interface is exposed on the root of the subdomain, the exact
  incantation that makes the media being exposed correctly is available in
  [`rproxy-kodi.conf`](Apps/nginx/site/rproxy-kodi.conf). This is based on the
  usual [Reverse Proxy nginx configuration](Apps/nginx.md).

  Note:
  - Follow the main nginx rproxy documentation
    - Assumes `<UPSTREAM-NAME>` is the Basic Authentication "name". It can and
      should be changed to some semi-unique value to your installation.
  - The `proxy_pass` URL must be configured exactly as-is. Include the trailing
    `/` for the top section, but remove it on the bottom. This has to do how
    the media URL are escaped, but this is guaranteed to work (dogfooding it
    myself).
  - The WebSocket proxying doesn't work, it will poll for updates. Good enough!
  - See "useful" links:
    - https://nginx.org/en/docs/http/websocket.html
    - https://github.com/xbmc/chorus2/issues/133

- ...
- Profit!

Make sure to use only the "Local" part of the interface (the pink stuff!),
unless you want to remotely prank people watching TV locally. There are issues
open to disable "local" control on the Web Interface:

- https://github.com/xbmc/chorus2/issues/384
- https://github.com/xbmc/chorus2/issues/318

[rpi4-guide]: https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4#installation
[arch-f2fs]: https://wiki.archlinux.org/title/F2FS
[somini-dotfiles]: https://gitlab.com/somini/dotfiles
[somini-dotnvim]: https://gitlab.com/somini/dotnvim
[somini-pacman]: https://pacman.somini.xyz/
