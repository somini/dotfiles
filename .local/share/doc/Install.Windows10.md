# Install Windows 10 (Basic Guide)

For when you need to use Windows (usually under duress), here's a basic survival guide.

## Install the Damn Things in the first place

You can download the ISO from here:

https://www.microsoft.com/en-us/software-download/windows10ISO

Use Rufus to create bootable USB drives

https://rufus.ie/

Obtaining a product key is outside the scope of this document.

# Restore some sanity

Use "O&O ShutUp 10" to restore some sanity to Windows. At least stops the worst
hair-pulling stuff.

https://www.oo-software.com/en/shutup10

Note that the executable must be copied to the disk, so that it can be re-run on updates.

# Extra software

- First Party (Microsoft utilities)
  - PowerToys: https://docs.microsoft.com/en-us/windows/powertoys/install

- Mozilla Firefox: https://www.mozilla.org/en-US/firefox/browsers/windows-64-bit/
  - uBlock Origin: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
  - Tree Style Tabs (Optional): https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/
- Thunderbird: https://www.thunderbird.net/en-US/thunderbird/all/
- 7zip: https://www.7-zip.org/
- Syncthing Client (SyncTrayzor): https://github.com/canton7/SyncTrayzor/releases
- SumatraPDF: https://www.sumatrapdfreader.org/free-pdf-reader.html

- Neovim: https://github.com/neovim/neovim/releases
