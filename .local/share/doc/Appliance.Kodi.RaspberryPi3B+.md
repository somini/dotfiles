# Install Kodi on a Raspberry Pi 3B+

## Installation Guide

Follow [the official guide to setup an ArchLinux ARM
system](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3).

For a Raspberry Pi 3B+, use the `ARMv7` version and **NOT** the `aarch64`
variant. This requires GPU usage, so we need full hardware support.

### Defaults

The defaults for the system images are:

- Hostname: `alarmpi`

- User `alarm`:`alarm`
- User `root`:`root`

### Followup: Follow the usual Arch Linux installation guide

See [the guide here](Install.ArchLinux.md). Some of the points do not apply.

### Enhancements: Logger

Use `kodi-logger` to log all the videos played on the device.

Simply install that package and run:

```
systemctl enable --now kodi-logger.timer
```

This will update the log file hourly.

Note that this doesn't log the music played, and the source log is ephemeral,
so this should be installed ASAP right after installation.

---

If you are mounting the kodi home directory using NFS, this requires some extra
tweaks, **before enabling the timer**.

- Create the file on the NAS itself, `chown` to `kodi:kodi`
- Edit the systemd service file:

```
systemctl edit kodi-logger.service
# Inside, add the following lines
[Service]
User=kodi
Group=kodi
PrivateTmp=yes
```

- Create the symlink to the location

Replace `~kodi` with the NFS location.

```
# /etc/tmpfiles.d/kodi-logger.conf
L /var/log/kodi-watched.log - - - - ~kodi/kodi-watched.log
```

To create the symlink right away, use:

```
systemd-tmpfiles --create
```
