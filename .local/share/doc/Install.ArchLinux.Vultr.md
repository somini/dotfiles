# Install Arch Linux on a Vultr VPS

**TL;DR**: Use another host for cloud Arch Linux machines. Maybe Linode or Hetzner.

Vultr *technically* support Arch Linux, but their version is old, this would
probably have issues with keeping it updated.

https://www.vultr.com/features/upload-iso/

Another issue is that they provide noVNC for remote access to the machines, but
they might be using an old version, non US keyboard layouts are not supported
in any way.
The fixed issue is here: https://github.com/novnc/noVNC/issues/21

The workaround is to use a custom ISO with the latest ISO and perform a regular
remote installation.
You can limp into enabling the SSH server on the installation environment using
tab completion and vim.

See:

- [Remote Arch Linux installation](https://wiki.archlinux.org/index.php/Install_Arch_Linux_via_SSH)

## Tutorial

- Change the root password

This is not set by default. Use a strong-ish password, it's not very important.

- Enable the SSH daemon

```
# systemctl start sshd
```

- Expire the root password

This will guarantee nobody else can enter. Use:

```
# passwd -e -l root
```

### Regular installation tweaks

Follow the regular installation guide [here](Install.ArchLinux.md)

### Bootloader

The bootloader is GRUB, on BIOS mode. The standard stuff will do.

### Network Configuration

The network configuration is a bog-standard DHCP. See
https://wiki.archlinux.org/index.php/Systemd-networkd#Basic_usage

- IPv4:
  - DHCP, the "static" IP will come here
  - Ignore hostname changes
- IPv6:
  - Accept Router Advertisements
  - Since this is a server, use `prefer-public` on the privacy extensions

These are the relevant settings on the network configuration (see
`systemd.network(5)`):

```
[Network]
DHCP=ipv4
IPv6AcceptRA=true
# This is a server
IPv6PrivacyExtensions=prefer-public

[DHCPv4]
SendHostname=no
UseHostname=no
```

After a reboot, just add both IP addresses to DNS, they should be stable.
There's always DDNS, if it's cheaper to have them dynamic.

### Create a `sysadmin` user

- Setup sudo, allow everyone on the `wheel` group to run everything
  - This is commented on the default sudoers
- `useradd -m -G wheel sysadmin`
  - `passwd sysadmin` into a password, doesn't have to be *THAT* strong since it will be protected by the SSH key

### Setup ssh on first boot

Otherwise it's impossible to remotely login on the first boot.

```
# systemctl enable sshd
```

### Setup ssh after DNS

Once DNS is configured, block any `root` logins and all password SSH logins.
Use only key-based authentication.
This requires having a key generated for `sysadmin` access. Use `sshimple-setup` for this.

Read https://infosec.mozilla.org/guidelines/openssh#Modern_.28OpenSSH_6.7.2B.29
and edit `/etc/ssh/sshd_config` to set:

```
PermitRootLogin no
MaxAuthTries 2
AuthenticationMethods publickey # Only pubkey auth
```
