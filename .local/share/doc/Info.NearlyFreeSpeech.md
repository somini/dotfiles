# Nearly Free Speech

NFS is a low-cost managed web application service. It is really nearly free.

https://members.nearlyfreespeech.net

## Tasks

### Setup TLS on a site

To setup TLS on a site, using Let's Encrypt:

1. Make sure the site is working as intended over the `*.nfshost.com` domain.

1. Setup the intended alias

2. Login on the host using SSH

2. Run `tls-setup.sh` to setup TLS for the aliases.

   This helper script does everything, including setting up a scheduled task to
   renew the certificates.
