#!/bin/bash
set -e
rootdir="$HOME/.local/share/ssh"

cs_file="$rootdir/connection_strings"
rofi_file="$HOME/.cache/rofi-2.sshcache"

usage() {
	cat >&2 <<-HELP
	Usage: ${0##*/} WHAT

	Tear down an existing SSH connection.

	Stores all data in '${rootdir/$HOME/\~}'.
	HELP
}

what="$1"
if ! [[ "$what" =~ "@" ]]; then
	usage
	exit 1
fi

for kp in "$rootdir/$what.sshkey"*; do
	if [ -f "$kp" ]; then
		echo "# Remove Key '${kp/$HOME/\~}'" >&2
		rm -f "$kp"
	fi
done

cs_line="Host $what"
if grep -qF "$cs_line" "$cs_file"; then
	tmp_file="$(mktemp)"
	echo "# Remove Connection String" >&2
	grep -vF "$cs_line" <"$cs_file" >"$tmp_file"
	cat <"$tmp_file" >"$cs_file"
	rm -f "$tmp_file"
fi

if [ -f "$rofi_file" ]; then
	if grep -qF "$what" "$rofi_file"; then
		tmp_file="$(mktemp)"
		echo "# Remove rofi Cache" >&2
		grep -vF " $what" <"$rofi_file" >"$tmp_file"
		cat <"$tmp_file" >"$rofi_file"
		rm -f "$tmp_file"
	fi
fi

# Removed
exit 0
