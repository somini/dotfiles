#!/bin/sh
set -e
set -o pipefail

echoerr() {
	echo "$@" >&2
}

TARSNAP_KEY="$HOME/.local/share/tarsnap.key"
TARSNAP_CACHE_MARKER="$HOME/.cache/tarsnap/directory"

if [ -f "$TARSNAP_KEY" ]; then
	echoerr "# Restore an Existing Key"
	tarsnap --fsck
else
	tarsnap_user="$(pass-field user Tarsnap)"
	echoerr "# Generate a New Key [User: $tarsnap_user]"
	tarsnap-keygen --keyfile "$TARSNAP_KEY" \
		--user "$tarsnap_user" \
		--machine "$(hostnamectl status --static)"
	if [ ! -f "$TARSNAP_CACHE_MARKER" ]; then
		echoerr "# Initialize Cache"
		tarsnap --initialize-cachedir
	else
		echoerr "# Cache Initialized"
	fi
fi
echoerr "# Double Checking tarsnap is configured correctly ..."
tarsnap --print-stats
echoerr "# ... DONE"
